# Generate release environment file

```sh tab="OTP"
 ./bin/pleroma_ctl release_env gen
```

```sh tab="From Source"
mix pleroma.release_env gen
```
